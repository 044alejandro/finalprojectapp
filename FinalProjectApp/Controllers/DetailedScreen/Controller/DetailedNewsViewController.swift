//
//  DetailedNewsViewController.swift
//  FinalProjectApp
//
//  Created by Виктор Сирик on 01.12.2021.
//

import UIKit

class DetailedNewsViewController: UIViewController {
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    var article: Articles?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        titleLabel.text = article?.title ?? ""
        detailsLabel.text = article?.summary ?? ""
        if let media = article?.media, let image = setImage(imageURL: media) {
            photoImage.image = image
        } else {
            photoImage.image = UIImage(named: "news")
        }
        
    }

    func setImage(imageURL: String) -> UIImage? {
        var image: UIImage?
        guard let url = URL(string: imageURL) else {
            return nil
        }
        do{
            let data = try Data(contentsOf: url, options: [])
            image = UIImage(data: data)
        } catch {
            return nil
        }
        return image
    }
    
    @IBAction func backAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
