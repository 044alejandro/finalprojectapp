//
//  NewsViewController + Extensions.swift
//  FinalProjectApp
//
//  Created by Виктор Сирик on 01.12.2021.
//

import Foundation
import UIKit

extension NewsViewController: UITableViewDataSource, UITableViewDelegate, NewsCellDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return news.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        cell.tag = indexPath.row
        cell.update(news: news[indexPath.row])
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == news.count - 1 && news.count < paginstion.total_hits {
            paginstion.page += 1
            loadData()
        }
    }
    
    func loadData() {
        RestData().loadData(q: self.q, pagination: self.paginstion){ data, error in
            guard error == nil else {
                print("error")
                return
            }
            guard let data = data else {
                print("error")
                return
            }
            self.paginstion = data
            self.news += data.articles
            DispatchQueue.main.async {
             self.newsTableView.reloadData()
             }
        }
    }
    
    func showDetails(index: Int) {
        let detailsVC = DetailedNewsViewController(nibName: "DetailedNewsViewController", bundle: nil)
        detailsVC.article = news[index]
        navigationController?.pushViewController(detailsVC, animated: true)
    }
  
}

extension NewsViewController: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        paginstion = NewsModel(articles: [], page: 1, page_size: 10, status: "", total_hits: 0, total_pages: 0)
        news = []
        loadData()
    }
    
}
