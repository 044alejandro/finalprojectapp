//
//  NewsViewController.swift
//  FinalProjectApp
//
//  Created by Виктор Сирик on 01.12.2021.
//

import UIKit

class NewsViewController: UIViewController {

    
    @IBOutlet weak var newsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var news: [Articles] = []
    var q = "world"
    var paginstion: NewsModel = NewsModel(articles: [], page: 1, page_size: 25, status: "", total_hits: 0, total_pages: 0)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        newsTableView.dataSource = self
        newsTableView.delegate = self
        searchBar.delegate = self
        newsTableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
    }
    @IBAction func searchAction(_ sender: Any) {
        q = searchBar.text ?? "world"
        loadData()
        searchBar.text = ""
    }

}

