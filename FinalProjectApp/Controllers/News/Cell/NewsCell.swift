//
//  NewsCell.swift
//  FinalProjectApp
//
//  Created by Виктор Сирик on 01.12.2021.
//

import UIKit

protocol NewsCellDelegate {
    func showDetails(index: Int)
}

class NewsCell: UITableViewCell {

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsTimeLabel: UILabel!
    var delegate: NewsCellDelegate?
    
    func setImage(imageURL: String) -> UIImage? {
        var image: UIImage?
        guard let url = URL(string: imageURL) else {
            return nil
        }
        do{
            let data = try Data(contentsOf: url, options: [])
            image = UIImage(data: data)
        } catch {
            return nil
        }
        return image
    }
    
    func update(news: Articles) {
        newsTitleLabel.text = news.title
        newsTimeLabel.text = news.published_date
        if let image = setImage(imageURL: news.media) {
            newsImage.image = image
        } else {
            newsImage.image = UIImage(named: "news")
        }
    }
    
    @IBAction func showDetailsAction(_ sender: Any) {
        delegate?.showDetails(index: tag)
    }
}

