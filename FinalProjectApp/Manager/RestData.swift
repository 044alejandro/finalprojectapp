//
//  RestData.swift
//  FinalProjectApp
//
//  Created by Виктор Сирик on 01.12.2021.
//

import Foundation
import Alamofire


class RestData{

    func loadData( q: String, pagination: NewsModel, completionHandler: @escaping (NewsModel?, String?) -> Void) {
    let urlPath = "https://free-news.p.rapidapi.com/v1/search"
        let parameters: [String : Any] = ["q" : q, "page" : pagination.page, "page_size" : pagination.page_size]
    NetworkManager.shared.loadData(urlPath: urlPath, method: .get, parameters: parameters) { data, error in
        
        guard error == nil else {
            completionHandler(nil, error)
            return
        }
        guard let data = data else {
            completionHandler(nil, nil)
            return
        }
        
        do {
            let decoder = JSONDecoder()
            let response = try decoder.decode(NewsModel.self, from: data)
            completionHandler(response, nil)
        } catch let errorRes as NSError {
            completionHandler(nil, errorRes.localizedDescription)
        }
    }
}
}
